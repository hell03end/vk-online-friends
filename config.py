""" This is an example of config file for app """

import os
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d]"
           " %(message)s",
    datefmt="%H:%M:%S"
)


# configure flask_wtf
WTF_CSRF_ENABLED = True
SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", "super-hard-2-guess-KEY")


# configure vk api
VK_OAUTH_URL = r"https://oauth.vk.com/"
VK_API_URL = r"https://api.vk.com/method/"
VK_API_V = 5.62
VK_API_LANG = "ru"
VK_ENV_VAR_NAME = "API_KEY_VK"
VK_ENV_VAR_SECRET_NAME = "API_SECRET_KEY_VK"

# change this values to valid one
API_KEY_VK = 1234560
API_SECRET_KEY_VK = "!invalid_secret_key!"


FOOTER_LINKS = [
    {'name': "Find error?",
     'url': "https://github.com/hell03end/vk-online-friends/issues/new"},
    {'name': "Contact me", 'url': "https://t.me/hell03end"}
]
