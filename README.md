vk-online-friends
=================
**THIS IS LEGACY PROGECT!**

Web-page (**[visit](https://vk-online-friends.herokuapp.com/login)**) for tracking online friends in VKontakte social network.

*Goal of development: learn how to create apps with usage of [flask framework](http://flask.pocoo.org/).*


Requirements
------------

* **Python** >= 3.6
* Account on **vk.com**


Usage
-----

* Download repository with: `git clone https://github.com/hell03end/vk-online-friends.git`.
* Install following enviroment variables (or write them into config.py):
  * `API_KEY_VK` - ID of vk app.
  * `API_SECRET_KEY_VK` - security key of vk app.
  * `FLASK_SECRET_KEY` - flask-forms secret key (any string).
* Create virtual env: `python -m venv [.venv]` (It's possible to use any name instead of `.venv`).
* Install dependencies with `pip install -r requirements.txt`.
* Start app with `python run.py`. App will be started at `localhost:5000` by default.


Examples
--------

```shell
python run.py -h
# usage: run.py [-h] [--host HOST] [--port PORT] [--debug DEBUG]

# Starts flask server.

# optional arguments:
#   -h, --help            show this help message and exit
#   --host HOST, -H HOST  Specify host id.
#   --port PORT, -p PORT  Specify local port, where to start server.
#   --debug DEBUG, -d DEBUG
#                         Start server in debug mode.
```


Visual Illustrations
--------------------

**Login page.**
![Login Page](https://gitlab.com/hell03end/vk-online-friends/raw/master/rcs/img/login.png)

**Main page** - track online friend for provided user-id.
![Main Page](https://gitlab.com/hell03end/vk-online-friends/raw/master/rcs/img/index.png)

**Mobile version** of app (login page).
![Mobile Login Page](https://gitlab.com/hell03end/vk-online-friends/raw/master/rcs/img/mobile.png)
