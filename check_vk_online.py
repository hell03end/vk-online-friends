from argparse import ArgumentParser

from vk_helpers.utils import exit_with_error
from vk_helpers.vk import find_online_friends, get_auth_vk_session


def parse_argv():
    parser = ArgumentParser(description="Show list of vk online-friends for"
                                        " user with specified id.")
    parser.add_argument('--id', '-i', type=int, default=None,
                        help="Id of user to search.")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_argv()
    auth_api = get_auth_vk_session()
    online_friends = find_online_friends(auth_api, args.id)

    if not online_friends:
        exit_with_error("Can't receive list of online friends.")

    for friend in online_friends:
        print(f"{friend['first_name']} {friend['last_name']} link: "
              f"https://vk.com/id{friend['id']}")
