from flask_wtf import FlaskForm
from wtforms import IntegerField


class IdInputForm(FlaskForm):
    user_id = IntegerField('user_id')
