import logging

from app import app
from app.forms import IdInputForm
from flask import flash, redirect, render_template, request, session, url_for
from vk_helpers.vk import (VK_ACCESS_RIGHTS, create_vk_api_session,
                           find_online_friends,
                           get_vk_auth_code_flow_access_token,
                           get_vk_auth_code_flow_url)

FOOTER_LINKS = app.config['FOOTER_LINKS']


def get_base_params() -> dict:
    return {
        'title': "Online Friends",
        'auth_session': 'token' in session,
        'footer_links': FOOTER_LINKS,
        'links_amount': len(FOOTER_LINKS)
    }


@app.route('/', methods=["GET", "POST"])
@app.route('/index', methods=["GET", "POST"])
def index():
    page_params = get_base_params()
    if 'token' not in session:
        logging.debug("User isn't logged in. Redirected to logging page")
        flash("You should log in first.")
        return redirect("login")
    auth_api = create_vk_api_session(session['token'])
    page_params['user'] = auth_api.users.get()[0]
    login_form = IdInputForm()
    page_params['form'] = login_form
    logging.info("Logged as user: '%s'", page_params['user'])

    target_id = -1
    if login_form.validate_on_submit():
        target_id = int(login_form.data.get('user_id', -1))

    if target_id < 1:
        logging.warning("Wrong user id specified!")
        flash("Wrong user id specified!")
        return render_template('index.html', **page_params)

    friends_online = find_online_friends(auth_api, target_id)
    page_params['online_friends'] = friends_online
    page_params['friends_amount'] = len(friends_online)
    logging.debug(
        "found %d online friends for user '%s'",
        page_params['friends_amount'],
        page_params['user']
    )

    return render_template('index.html', **page_params)


@app.route('/login', methods=["GET", "POST"])
def login():
    if "token" in session:
        logging.debug("User already logged in. Redirect to index page")
        return redirect(url_for('index'))
    auth_uri = get_vk_auth_code_flow_url(
        callback_url=f"{request.url_root[:-1]}{url_for('login_vk')}",
        scope_params=VK_ACCESS_RIGHTS['friends']
    )
    page_params = get_base_params()
    page_params['title'] = "Login Page"
    page_params['login_url'] = auth_uri
    return render_template("login.html", **page_params)


@app.route('/login/vk', methods=["GET", "POST"])
def login_vk():
    auth_code = request.args.get('code', None)
    if not auth_code:
        logging.debug("Can't receive auth code from vk! "
                      "Redirect to logging page")
        flash("<b>Authentication failed</b>: can't receive access code.")
        return redirect(url_for('login'))
    logging.info("Receive auth code: %s", auth_code)

    access_token = get_vk_auth_code_flow_access_token(
        access_code=auth_code,
        callback_url=f"{request.url_root[:-1]}{url_for('login_vk')}"
    )
    if not access_token:
        logging.debug("Can't receive access token from vk! "
                      "Redirect to logging page")
        flash("<b>Authentication failed</b>: can't receive access token.")
        return redirect(url_for('login'))
    session['token'] = access_token

    logging.info("Receive access token: %s", access_token)
    return redirect(url_for('index'))


@app.route('/about')
def about():
    page_params = get_base_params()
    return render_template("about.html", **page_params)


@app.route("/logout")
def logout():
    session.pop("token", None)
    logging.debug("Logout from app.")
    return redirect(url_for("login"))
