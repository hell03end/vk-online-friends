import json
import logging
import os
import re
import sys
from distutils.util import strtobool
from getpass import getpass

import requests


def get_json_form_request(base_url: str,
                          endpoint: str='',
                          request_type: str="POST",
                          **params) -> dict:
    url = f"{base_url}{endpoint}"
    try:
        if request_type.upper() == "POST":
            return requests.post(url, params=params).json()
        return requests.get(url, params=params).json()
    except BaseException as err:
        logging.error("Can't request '%s' with params: %s", url, params)
        raise err


def get_api_key(env_var_name: str,
                source_name: str,
                get_from_cli: bool=False,
                fallback: str="") -> str:
    api_key = os.environ.get(env_var_name, fallback)
    if not api_key and get_from_cli:
        api_key = getpass(f"Please, enter your api-key for {source_name}: ")
        if api_key:
            logging.info("Set env variable: %s", env_var_name)
            os.environ[env_var_name] = api_key
    return api_key


def get_user_credentials_from_cli() -> dict:
    return {
        'login': input("Login:\t\t"),
        'password': getpass("Password:\t")
    }


def create_valid_url(base_url: str,
                     endpoint: str='',
                     method: str="GET",
                     **params) -> str:
    return requests.Request(
        method=method.lower(),
        url=f"{base_url}{endpoint}",
        params=params
    ).prepare().url


def ask_to_continue_from_cli(question: str) -> bool:
    print(question)
    answer = input("Proceed ([y]/n)? ")
    while not re.fullmatch(r"^(yes|y|no|n)$", answer):
        print(f"Invalid choice: {answer}")
        answer = input("Proceed ([y]/n)? ")
    answer = strtobool(answer)
    logging.debug("User decide to continue? %s", answer)
    return answer


def save_json_data(data: dict or list,
                   path: str,
                   method: str="write",
                   encoding: str="utf-8",
                   errors: str="xmlcharrefreplace") -> None:
    if os.path.exists(path) and \
            not ask_to_continue_from_cli("Path already exists, rewrite?"):
        logging.debug("Data wasn't save to: %s", path)
        return
    with open(path, method[0], encoding=encoding, errors=errors) as fout:
        json.dump(data, fout)
    logging.debug("Data was saved at: %s", path)


def load_json_data(path: str,
                   encoding: str="utf-8",
                   errors: str="xmlcharrefreplace") -> (dict, list, None):
    if os.path.exists(path):
        with open(path, encoding=encoding, errors=errors) as file:
            logging.debug("Loading data from: %s", path)
            return json.load(file)
    logging.debug("'%s' wasn't founded", path)


def exit_with_error(message: str="error", code: int=1) -> None:
    logging.error(message)
    sys.exit(code)
