import logging
import time

import vk
from config import (API_KEY_VK, API_SECRET_KEY_VK, VK_API_LANG, VK_API_URL,
                    VK_API_V, VK_ENV_VAR_NAME, VK_ENV_VAR_SECRET_NAME,
                    VK_OAUTH_URL)
from selenium.webdriver import Chrome
from vk_helpers.utils import (create_valid_url, get_api_key,
                              get_json_form_request,
                              get_user_credentials_from_cli)

VK_ACCESS_RIGHTS = {
    'notify': 1,
    'friends': 2,
    'photos': 4,
    'audio': 8,
    'video': 16,
    'pages': 128,
    'quick_link': 256,
    'status': 1024,
    'notes': 2048,
    'messages': 4096,
    'wall': 8192,
    'ads': 32768,
    'offline': 65536,
    'docs': 131072,
    'groups': 262144,
    'notifications': 524288,
    'stats': 1048576,
    'email': 4194304,
    'market': 134217728
}
API_KEY_VK = get_api_key(
    env_var_name=VK_ENV_VAR_NAME,
    source_name="vk",
    fallback=API_KEY_VK
)


def get_json_data_from_vk(endpoint: str,
                          base_url: str=VK_API_URL,
                          **params) -> dict:
    return get_json_form_request(
        base_url=base_url,
        endpoint=endpoint,
        request_type="GET",
        **params
    )


def get_auth_vk_api_session(api_key: str,
                            user_credentials: dict,
                            scope_params: object,
                            version: float=VK_API_V,
                            lang: str=VK_API_LANG) -> object:
    auth_session = vk.AuthSession(
        app_id=api_key,
        user_login=user_credentials['login'],
        user_password=user_credentials['password'],
        scope=scope_params
    )
    return vk.API(auth_session, v=version, lang=lang)


def create_vk_api_session(access_token: str,
                          lang: str=VK_API_LANG,
                          version: float=VK_API_V) -> object:
    auth_session = vk.Session(access_token=access_token)
    return vk.API(auth_session, v=version, lang=lang)


def get_vk_service_token(api_key: str,
                         secure_key: str,
                         version: float=VK_API_V) -> dict:
    params = {
        'client_id': api_key,
        'client_secret': secure_key,
        'grant_type': "client_credentials",
        'v': version
    }
    return get_json_form_request(
        base_url=VK_OAUTH_URL,
        endpoint="access_token",
        **params
    )


def get_access_token_from_url(url: str) -> str:
    return url[45:].split(r"&")[0]


def get_vk_access_token(url: str, user_credentials: dict) -> str:
    browser = Chrome()
    try:
        browser.get(url)
        logging.debug("Loading '%s' in browser...", url)
        login_form = browser.find_element_by_id('login_submit')

        form_username = login_form.find_element_by_name('email')
        form_username.clear()
        form_username.send_keys(user_credentials['login'])

        form_password = login_form.find_element_by_name('pass')
        form_password.clear()
        form_password.send_keys(user_credentials['password'])

        login_form.submit()
        if "access_token=" not in browser.current_url:
            logging.warning("Can't get access token automatically!")
            login_form = browser.find_element_by_id('login_submit')
            form_password = login_form.find_element_by_name('pass')
            form_password.send_keys(user_credentials['password'])

        while "access_token=" not in browser.current_url:
            logging.debug("Trying to get access token from vk...")
            time.sleep(0.25)
        access_token_url = browser.current_url
    except BaseException as err:
        logging.error(err)
        raise err
    finally:
        logging.debug("Closing browser...")
        browser.close()

    return get_access_token_from_url(access_token_url)


def get_auth_vk_api_session_selenium(api_key: str,
                                     user_credentials: dict,
                                     scope_params: object,
                                     lang: str=VK_API_LANG,
                                     version: float=VK_API_V,
                                     display: str="popup") -> object:
    auth_params = {
        'client_id': api_key,
        'redirect_uri': create_valid_url(VK_OAUTH_URL, "blank.html"),
        'display': display,
        'scope': scope_params,
        'response_type': "token",
        'revoke': 0,
        'v': version
    }
    requested_url = create_valid_url(
        base_url=VK_OAUTH_URL,
        endpoint="authorize",
        **auth_params
    )
    access_token = get_vk_access_token(requested_url, user_credentials)
    return create_vk_api_session(access_token, lang=lang)


def get_auth_vk_session(user_credentials: dict=None):
    api_key = API_KEY_VK
    if not user_credentials:
        user_credentials = get_user_credentials_from_cli()
    scope_params = VK_ACCESS_RIGHTS['friends'] | VK_ACCESS_RIGHTS['pages'] | \
        VK_ACCESS_RIGHTS['wall'] | VK_ACCESS_RIGHTS['groups']
    try:
        return get_auth_vk_api_session(
            api_key,
            user_credentials,
            scope_params
        )
    except vk.exceptions.VkAuthError as err:
        logging.warning(err)
        return get_auth_vk_api_session_selenium(
            api_key,
            user_credentials,
            scope_params
        )


def get_vk_auth_code_flow_url(callback_url: str,
                              scope_params: int,
                              version: str=VK_API_V,
                              display: str="popup") -> str:
    api_key = API_KEY_VK
    auth_params = {
        'client_id': api_key,
        'redirect_uri': callback_url,
        'display': display,
        'scope': scope_params,
        'response_type': "code",
        'v': version
    }
    return create_valid_url(
        base_url=VK_OAUTH_URL,
        endpoint="authorize",
        **auth_params
    )


def get_vk_auth_code_flow_access_token(access_code: str,
                                       callback_url: str) -> str:
    auth_params = {
        'client_id': API_KEY_VK,
        'client_secret': get_api_key(
            env_var_name=VK_ENV_VAR_SECRET_NAME,
            source_name="vk secret",
            fallback=API_SECRET_KEY_VK
        ),
        'redirect_uri': callback_url,
        'code': access_code,
    }
    response = get_json_form_request(
        base_url=VK_OAUTH_URL,
        endpoint="access_token",
        **auth_params
    )
    return response.get('access_token', "")


def specify_params(user_id: int=None) -> dict:
    params = {
        'order': "name",
        'fields': "photo_100,online"
    }
    if user_id:
        params['user_id'] = user_id
    return params


def find_online_friends(api: object, user_id: int=None) -> list:
    online_friends = []
    params = specify_params(user_id)
    response = api.friends.get(**params)
    if "error" in response:
        logging.warning("Error was found in response: %s", response['error'])
        return []
    for friend in response['items']:
        if friend['online']:
            online_friends.append(friend)
    return online_friends
